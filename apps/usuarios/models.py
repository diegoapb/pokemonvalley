from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Entrenador (models.Model):
    nombre = models.CharField(max_length=50)
    nivel = models.IntegerField()
    imgURL = models.URLField(max_length=300)
    fk_user = models.OneToOneField(User,blank=True, null=True,on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.nombre)
