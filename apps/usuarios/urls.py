#from django.conf.urls import url
#from rest_framework.urlpatterns import format_suffix_patterns
#from apps.usuarios import views

#urlpatterns = [
#    url(r'^usuarios/$',views.UserList.as_view(),name='ususarios_listar'),
#    url(r'^usuarios/(?P<pk>[0-9]+)/$',views.UserDetail.as_view(),name='ususarios_detail'),
#
#]
#
#urlpatterns = format_suffix_patterns(urlpatterns)

from django.urls import path
from apps.usuarios import views
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

app_name = 'usuarios'

urlpatterns = [
    path('us/',views.UserList.as_view()),
]