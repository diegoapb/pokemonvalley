from django.shortcuts import render
from django.contrib.auth.models import User
from apps.usuarios.serializers import UserSerializers
from rest_framework import generics
# Create your views here.

class UserList (generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializers

class UserDetail(generics.RetrieveDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializers