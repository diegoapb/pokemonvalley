from django.db import models
from apps.usuarios.models import Entrenador
# Create your models here.



class Move (models.Model):
    name = models.CharField(max_length=50)
    power = models.IntegerField(null=False,blank=True)
    pp = models.IntegerField(null=False,blank=True)
    pp_state = models.IntegerField(null=False,blank=True)

    def __str__(self):
        return '{}'.format(self.name)

class Pokemon (models.Model):
    fk_entrenador_id = models.ForeignKey(Entrenador,null=False,blank=False,on_delete=models.CASCADE)
    moves = models.ManyToManyField(Move,blank=True)
    id_pokedex = models.IntegerField(null=False,blank=False)
    name = models.CharField(max_length=50)
    hp =models.IntegerField(null=False,blank=False)
    hp_state = models.IntegerField(null=False)
    attack = models.IntegerField(null=False, blank=False)
    defense = models.IntegerField(null=False, blank=False)

    def __str__(self):
        return '{}'.format(self.name)

