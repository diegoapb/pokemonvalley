from django.contrib import admin
from apps.pokemon.models import Pokemon,Move
# Register your models here.

admin.site.register(Pokemon)
admin.site.register(Move)