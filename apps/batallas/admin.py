from django.contrib import admin
from apps.batallas.models import Winner, Batalla
# Register your models here.

admin.site.register(Batalla)
admin.site.register(Winner)
