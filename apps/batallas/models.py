from django.db import models
from apps.usuarios.models import Entrenador
# Create your models here.

class Batalla (models.Model):
    entrenadores = models.ManyToManyField(Entrenador,blank=True)
    level=models.IntegerField(blank=True)
    date_start=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        nombre_entrenador = ", ".join(str(seg) for seg in self.entrenadores.all())
        return '{}'.format(nombre_entrenador)

class Winner (models.Model):
    winner = models.ForeignKey(Entrenador,on_delete=models.CASCADE)
    batalla = models.OneToOneField(Batalla,on_delete=models.CASCADE)

    def __str__(self):
        return'{}{}'.format(self.winner.nombre,self.batalla.date_start)